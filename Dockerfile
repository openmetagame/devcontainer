ARG NODE_VERSION="none"
ARG GO_VERSION="none"
ARG VARIANT="bookworm"
ARG DENO_VERSION="none"

FROM node:${NODE_VERSION}-${VARIANT} AS node
COPY node.sh versions.env  /tmp/install_scripts/
RUN cd  /tmp/install_scripts && ./node.sh && rm -rf  /tmp/install_scripts

FROM golang:${GO_VERSION}-${VARIANT} AS golang
COPY go* versions.env  /tmp/install_scripts/
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    cd /tmp/install_scripts && ./go.sh && rm -rf /tmp/install_scripts

FROM debian:${VARIANT} AS deno
COPY deno* versions.env  /tmp/install_scripts/
RUN cd  /tmp/install_scripts && ./deno.sh && rm -rf  /tmp/install_scripts

FROM mcr.microsoft.com/vscode/devcontainers/base:${VARIANT}

#speedup the build by removing the man auto-update
RUN rm -rf /var/lib/man-db/auto-update

RUN apt clean && apt-get update
RUN apt-get install -y apt-transport-https ca-certificates gnupg sudo unzip make wget git-lfs python3 python3-pip pipx libc6 xz-utils && rm -rf /var/lib/apt/lists/*

RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee /etc/apt/sources.list.d/google-cloud-sdk.list

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

RUN curl -fsSL https://pkg.cloudflare.com/cloudflare-main.gpg | sudo tee /usr/share/keyrings/cloudflare-main.gpg >/dev/null
RUN echo "deb [signed-by=/usr/share/keyrings/cloudflare-main.gpg] https://pkg.cloudflare.com/cloudflared $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/cloudflared.list

RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list

RUN apt-get update && apt-get install -y google-cloud-sdk google-cloud-sdk-gke-gcloud-auth-plugin docker-ce-cli redis cloudflared terraform && rm -rf /var/lib/apt/lists/*

COPY --from=node /usr/local/ /usr/local/
COPY --from=golang /usr/local /usr/local
COPY --from=deno /usr/local /usr/local

RUN mkdir -p /go/bin && mkdir -p /tmp/install_scripts && mkdir -p /opt/openmetagame
ENV GOPATH=/go
ENV GOBIN=/go/bin
ENV DOTNET_ROOT=/opt/dotnet
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=0
ENV DOTNET_CLI_TELEMETRY_OPTOUT=1
ENV PATH=$GOBIN:$PATH:$DOTNET_ROOT:/usr/local/go/bin
ENV OMG_VERSIONS_DOTENV=/opt/openmetagame/versions.env

ARG OMG_DEVCONTAINER_TAG="latest"
ARG OMG_DEVCONTAINER_IMAGE="devcontainer"

COPY install.sh versions.env  /tmp/install_scripts/
RUN cd  /tmp/install_scripts && ./install.sh "${OMG_DEVCONTAINER_IMAGE}" "${OMG_DEVCONTAINER_TAG}"  && rm -rf  /tmp/install_scripts && rm -rf /var/lib/apt/lists/*