#!/bin/bash -e

export $(grep -v '^#' versions.env | xargs)

#setup some git defaults
git config --global user.email "gitlab@openmetagame.dev"
git config --global user.name "GitLab CI/CD"

ARCH=`uname -m`
PB_ARCH=$ARCH
PB_JS_ARCH="x86_64"
GRPC_ARCH="x64"
KUBECTRL_ARCH="amd64"

if [ $ARCH = "aarch64" ]
then
	PB_ARCH="aarch_64"
	PB_JS_ARCH="aarch_64"
	GRPC_ARCH="arm64"
	KUBECTRL_ARCH="arm64"
fi

echo 'install semgrep'
pipx install semgrep


echo 'install protoc to /usr/local'
PB_REL="https://github.com/protocolbuffers/protobuf/releases/download/v$PB_VERSION/protoc-$PB_VERSION-linux-$PB_ARCH.zip"
curl -LO $PB_REL
unzip -o protoc-$PB_VERSION-linux-$PB_ARCH.zip -d /usr/local
rm protoc-$PB_VERSION-linux-$PB_ARCH.zip

echo "install grpc_csharp_plugin"
curl -L https://www.nuget.org/api/v2/package/Grpc.Tools/$GRPC_TOOLS_VERSION -o temp.zip 
unzip -p temp.zip tools/linux_$GRPC_ARCH/grpc_csharp_plugin > /usr/local/bin/grpc_csharp_plugin
chmod +x /usr/local/bin/grpc_csharp_plugin 
rm temp.zip

echo "install protoc-gen-js"
curl -L "https://github.com/protocolbuffers/protobuf-javascript/releases/download/v$PB_JS_VERSION/protobuf-javascript-$PB_JS_VERSION-linux-$PB_JS_ARCH.zip" -o temp.zip
unzip -p temp.zip bin/protoc-gen-js > /usr/local/bin/protoc-gen-js
chmod +x /usr/local/bin/protoc-gen-js
rm temp.zip

echo "install aws sdk"
curl "https://awscli.amazonaws.com/awscli-exe-linux-$ARCH.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install

echo "install argocd"
curl -sSL -o argocd-temp https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-$KUBECTRL_ARCH
sudo install -m 555 argocd-temp /usr/local/bin/argocd


#eksctl
echo "install eksctl"
curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_Linux_$KUBECTRL_ARCH.tar.gz"
curl -sL "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_checksums.txt" | grep Linux_$KUBECTRL_ARCH | sha256sum --check
tar -xzf eksctl_Linux_$KUBECTRL_ARCH.tar.gz -C /tmp && rm eksctl_Linux_$KUBECTRL_ARCH.tar.gz
sudo mv /tmp/eksctl /usr/local/bin
eksctl version

#install minikube, kubectrl and helm
export VERSION="$KUBECTL_VERSION"
export MINIKUBE="none"
curl https://raw.githubusercontent.com/devcontainers/features/main/src/kubectl-helm-minikube/install.sh | bash

#dotnet
echo "Installng dotnet '$DOTNET_VERSION'"
wget https://dot.net/v1/dotnet-install.sh && chmod +x dotnet-install.sh
mkdir -p /opt/dotnet
./dotnet-install.sh --channel $DOTNET_VERSION --install-dir /opt/dotnet

# add the vscode user to docker
groupadd docker && usermod -aG docker vscode

# save the build info
cp versions.env $OMG_VERSIONS_DOTENV

#embed the image and tags into the version
echo "OMG_DEVCONTAINER_IMAGE=$1:$2" | tee -a $OMG_VERSIONS_DOTENV
echo "OMG_DEVCONTAINER_VERSION=$2" | tee -a $OMG_VERSIONS_DOTENV

#setup env for users
echo "export \$(grep -v '^#' $OMG_VERSIONS_DOTENV | xargs)" | tee -a $HOME/.bashrc
echo "export \$(grep -v '^#' $OMG_VERSIONS_DOTENV | xargs)" | tee -a /home/vscode/.bashrc

echo "cleanup permissions"

#cleanup permissions
chmod 755 -R /usr/local
chmod 755 -R /opt
chown vscode:vscode -R /go

echo "done"

