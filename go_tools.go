package main

import (
	_ "github.com/a-h/generate/cmd/schema-generate"
	_ "github.com/bitnami-labs/sealed-secrets/cmd/kubeseal"
	_ "github.com/cespare/reflex"
	_ "github.com/ckaznocha/protoc-gen-lint"
	_ "github.com/google/ko"
	_ "github.com/openmetagame/gocover-cobertura"
	_ "github.com/urfave/cli/v2"
	_ "github.com/yoheimuta/protolint/cmd/protolint"
	_ "golang.org/x/tools/cmd/stringer"
	_ "golang.org/x/vuln/cmd/govulncheck"
	_ "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
	_ "google.golang.org/protobuf/cmd/protoc-gen-go"
	_ "gotest.tools/gotestsum"
)
