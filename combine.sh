#!/bin/sh

#load in the versions file
export $(grep -v '^#' versions.env | xargs)

IMAGE="${CI_REGISTRY_IMAGE:-localhost}"
TAG="${CI_COMMIT_TAG:-latest}"

docker manifest create $IMAGE:$TAG \
--amend $IMAGE:$TAG-amd64 \
--amend $IMAGE:$TAG-arm64 

docker manifest push --purge $IMAGE:$TAG