#!/bin/bash -e

export $(grep -v '^#' versions.env | xargs)

apt-get update && apt-get install xz-utils

#install upx
ARCH=`uname -m`
UPX_ARCH=amd64
if [ $ARCH = "aarch64" ]
then
    UPX_ARCH="arm64"
fi

curl -sLO "https://github.com/upx/upx/releases/download/v${UPX_VERSION}/upx-${UPX_VERSION}-${UPX_ARCH}_linux.tar.xz"
tar -xf upx-${UPX_VERSION}-${UPX_ARCH}_linux.tar.xz -C /tmp && rm upx-${UPX_VERSION}-${UPX_ARCH}_linux.tar.xz
mv /tmp/upx-$UPX_VERSION-${UPX_ARCH}_linux/upx /usr/bin/upx

#install go tools to /usr/local/bin
export GOBIN=/usr/local/bin

#default tools
go install -ldflags="-s -w" golang.org/x/tools/gopls@latest
go install -ldflags="-s -w" honnef.co/go/tools/cmd/staticcheck@latest
go install -ldflags="-s -w" golang.org/x/lint/golint@latest
go install -ldflags="-s -w" github.com/mgechev/revive@latest
go install -ldflags="-s -w" github.com/uudashr/gopkgs/v2/cmd/gopkgs@latest
go install -ldflags="-s -w" github.com/ramya-rao-a/go-outline@latest
go install -ldflags="-s -w" github.com/go-delve/delve/cmd/dlv@latest
go install -ldflags="-s -w" github.com/golangci/golangci-lint/cmd/golangci-lint@latest
go install -ldflags="-s -w" github.com/jesseduffield/lazydocker@latest

go mod download
cat go_tools.go | grep _ | awk -F'"' '{print $2}' | xargs -tI % go install -ldflags="-s -w"  %

cd /usr/local/bin 
ls | xargs -tI % upx --lzma %

cp /usr/bin/upx /usr/local/bin/upx

chmod 755 -R /usr/local/bin
ls -alh /usr/local/bin