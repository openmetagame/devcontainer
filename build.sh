#!/bin/sh

#load in the versions file
export $(grep -v '^#' versions.env | xargs)

defaultArch='amd64'
arch=`uname -m`
if [ $arch = "aarch64" ]
then
	defaultArch="arm64"
fi

ARCH="${ARCH-$defaultArch}"
IMAGE="${CI_REGISTRY_IMAGE:-localhost}"
TAG="${CI_COMMIT_TAG:-latest}"

docker buildx build \
    --progress=plain \
    --platform linux/$ARCH \
    --build-arg NODE_VERSION="$NODE_VERSION" \
    --build-arg GO_VERSION="$GO_VERSION" \
    --build-arg DENO_VERSION="$DENO_VERSION" \
    --build-arg OMG_DEVCONTAINER_TAG="$TAG" \
    --build-arg OMG_DEVCONTAINER_IMAGE="$IMAGE" \
    --tag $IMAGE:$TAG-$ARCH \
    --provenance false \
    --push \
    .