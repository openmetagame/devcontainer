#!/bin/bash -e

export $(grep -v '^#' versions.env | xargs)

apt clean && apt-get update
apt install -y curl p7zip-full

export DENO_INSTALL=/usr/local

curl -fsSL https://deno.land/install.sh | sh -s "v$DENO_VERSION"