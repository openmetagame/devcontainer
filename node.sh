#!/bin/bash -e

export $(grep -v '^#' versions.env | xargs)

npm install -g npm@$NPM_VERSION

